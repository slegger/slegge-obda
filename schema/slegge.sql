-----------------------------------------------------
--- SCHEMA: "SLEGGE"
---
--- VIEWS FOR SLEGGE_EPI TABLES
----------------------------------------------------

--
CREATE VIEW REF_CLASSIFICATION_SYSTEM AS
SELECT R_CLSN_SYS_S as REF_CLASSIFICATION_SYSTEM_S,
    NAME
FROM SLEGGE_EPI.R_CLSN_SYS;


--
CREATE VIEW CLASSIFICATION_SYSTEM AS
SELECT KIND,
    CLSN_SYS_NAME as NAME
FROM SLEGGE_EPI.CLSN_SYS;


--
CREATE VIEW REFERENCE_DATA_COLLECTION AS
SELECT DATA_COLLECTION_S as IN_DATA_COLLECTION_S,
    REF_DATA_S
FROM SLEGGE_EPI.REFERENCE_DATA_X;


--
CREATE VIEW DATA_COLLECTION AS
SELECT DATA_COLLECTION_S,
    DATA_COLLECTION_NM as NAME,
    R_DATA_COLL_TY_NM as REF_DATA_COLLECTION_TYPE
FROM SLEGGE_EPI.DATA_COLLECTION;


--
CREATE VIEW EARTH_MODEL AS
SELECT EARTH_MODEL_S
FROM SLEGGE_EPI.EARTH_MODEL;


--
CREATE VIEW DATA_COLLECTION_CONTENT AS
SELECT E_AND_P_DATA_S as COLLECTION_PART_S,
    DATA_COLLECTION_S as PART_OF_S
FROM SLEGGE_EPI.COLL_CNTN_X;


--
CREATE VIEW EARTH_MODEL_COMPOSITION AS
SELECT EARTH_MODEL_S,
    SPATIAL_OBJECT_S
FROM SLEGGE_EPI.SPATIAL_OBJECT_X;


--
CREATE VIEW STRATIGRAPHIC_MARKER AS
SELECT STRAT_MRK_S as STRATIGRAPHIC_MARKER_S,
    ENTITY_TYPE_NM as ENTITY_TYPE_NAME,
    DESCRIPTION
FROM SLEGGE_EPI.STRAT_MRK;


--
CREATE VIEW FEATURE_BOUNDARY_PART AS
SELECT FEATURE_BND_PART_S as FEATURE_BOUNDARY_PART_S,
    FEATURE_BOUNDARY_S
FROM SLEGGE_EPI.FEATURE_BND_PART;


--
CREATE VIEW BOUNDARY_CLASSIFICATION AS
SELECT CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    DESCRIPTION,
    FEATURE_BND_S as FEATURE_BOUNDARY_S
FROM SLEGGE_EPI.BOUNDARY_CLSN;


--
CREATE VIEW RESERVOIR AS
SELECT RESERVOIR_S,
    RESERVOIR_NAME as NAME
FROM SLEGGE_EPI.RESERVOIR;


--
CREATE VIEW TYPICAL_ACTIVITY AS
SELECT TYPICAL_ACT_S as TYPICAL_ACTIVITY_S,
    KIND_S,
    TYPICAL_ACT_NAME as NAME,
    SUPER_KIND_S
FROM SLEGGE_EPI.TYPICAL_ACT;


--
CREATE VIEW REF_DESCRIPTIVE_PROPERTY AS
SELECT R_DSC_PTY_S as REF_DESCRIPTIVE_PROPERTY_S,
    R_PROPERTY_TYPE_NM as NAME
FROM SLEGGE_EPI.R_DSC_PTY;


--
CREATE VIEW REF_PROPERTY_KIND AS
SELECT R_PROPERTY_KIND_S as REF_PROPERTY_KIND_S,
    R_PROPERTY_TYPE_S as REF_PROPERTY_TYPE_S
FROM SLEGGE_EPI.R_PROPERTY_KIND;


--
CREATE VIEW PTY_QUANTITY AS
SELECT ENTITY_TYPE_NM as ENTITY_TYPE_NAME,
    MATERIAL_S,
    QUANTITY_VALUE,
    QUANTITY_VALUE_U,
    QUANTITY_VALUE_Q,
    R_PROPERTY_KIND_S as REF_PROPERTY_KIND_S
FROM SLEGGE_EPI.P_EQUIPMENT_FCL;


--
CREATE VIEW WELLBORE_POINT AS
SELECT WELLBORE_POINT_S,
    AZIMUTH,
    AZIMUTH_U,
    GEOLOGIC_FTR_S as GEOLOGIC_FEATURE_S,
    EMODEL_OBJECT_ID as IDENTIFIER,
    INCLINATION as INCLINATION_FROM_VERTICAL,
    INCLINATION_U as INCLINATION_FROM_VERTICAL_U,
    R_WELLBORE_PT_NAME as REF_WELLBORE_POINT,
    WELLBORE_S
FROM SLEGGE_EPI.WELLBORE_POINT;


--
CREATE VIEW ACTIVITY_CLASS AS
SELECT ACTIVITY_CLASS_S,
    CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    CLSN_CLS_NAME as NAME
FROM SLEGGE_EPI.ACTIVITY_CLASS;


--
CREATE VIEW ACTIVITY AS
SELECT ACTIVITY_S,
    CONTAINING_ACT_S as CONTAINING_ACTIVITY_S,
    FACILITY_S,
    FACILITY_T,
    IDENTIFIER,
    KIND_S,
    MATERIAL_S,
    MATERIAL_T,
    R_EXISTENCE_KD_NM as REF_EXISTENCE_KIND,
    SEIS_GEOM_SET_S as SEISMIC_GEOMETRY_SET_S
FROM SLEGGE_EPI.ACTIVITY;


--
CREATE VIEW BUSINESS_ASSOCIATE AS
SELECT BSASC_S as BUSINESS_ASSOCIATE_S,
    BSASC_ID as IDENTIFIER,
    KIND,
    R_NAMING_SYSTEM_KD as REF_NAMING_SYSTEM
FROM SLEGGE_EPI.BSASC;


--
CREATE VIEW BUSINESS_ASSOC_FACILITY_INVT AS
SELECT BSASC_FCL_ASN_S as BUSINESS_ASSOC_FACILITY_INVT_S,
    BSASC_S as BUSINESS_ASSOCIATE_S,
    FACILITY_S,
    FACILITY_T,
    INVOLVEMENT_ROLE
FROM SLEGGE_EPI.BSASC_FCL_ASN;


--
CREATE VIEW DOCUMENT_SPECIFICATION_CLASS AS
SELECT DOC_SPEC_CLS_S DOCUMENT_SPECIFICATION_CLASS_S,
    CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    CLSN_CLS_NAME as NAME
FROM SLEGGE_EPI.DOC_SPEC_CLS;


--
CREATE VIEW DOCUMENT_SPECIFICATION_CLSN AS
SELECT DOC_SPEC_CLSN_S as DOCUMENT_SPECIFICATION_CLSN_S,
    DOCUMENT_SPEC_S as DOCUMENT_SPECIFICATION_S,
    DOCUMENT_SPEC_T as DOCUMENT_SPECIFICATION_T,
    DOC_SPEC_CLS_S as DOCUMENT_SPECIFICATION_CLASS_S,
    DOC_SPEC_CLS_NAME as DOCUMENT_SPEC_CLASS_NAME
FROM SLEGGE_EPI.DOC_SPEC_CLSN;


--
CREATE VIEW DOCUMENT_SPEC_BUSINESS_ASSOC AS
SELECT DOC_SPEC_BSASC_S as DOCUMENT_SPEC_BUSINESS_ASSOC_S,
    BSASC_S as BUSINESS_ASSOCIATE_S,
    BSASC_ID as BUSINESS_ASSOCIATE_IDENTIFIER,
    DOC_SPEC_S as DOCUMENT_SPECIFICATION_S,
    DOC_SPEC_T as DOCUMENT_SPECIFICATION_T,
    ROLE
FROM SLEGGE_EPI.DOC_SPEC_BSASC;


--
CREATE VIEW TYPICAL_DOCUMENT_SPEC AS
SELECT TYP_DOC_SPEC_S as TYPICAL_DOCUMENT_SPEC_S,
    DESCRIPTION
FROM SLEGGE_EPI.TYP_DOC_SPEC;


--
CREATE VIEW LICENSE_AGREEMENT AS
SELECT LICENSE_AGR_S as LICENSE_AGREEMENT_S,
    DOCUMENT_SPEC_ID as IDENTIFIER,
    EXECUTION_DATE,
    EXPIRY_DATE,
    ORIGINAL_EXPIRY_DT as ORIGINAL_EXPIRY_DATE,
    R_NAMING_SYSTEM_KD as REF_NAMING_SYSTEM
FROM SLEGGE_EPI.LICENSE_AGR;


--
CREATE VIEW SUBJECT_OF_CONTRACT AS
SELECT SUBJ_OF_CTRT_X_S as SUBJECT_OF_CONTRACT_S,
    BUSINESS_OBJECT_S,
    BUSINESS_OBJECT_T,
    CONTRACT_S,
    CONTRACT_T
FROM SLEGGE_EPI.SUBJ_OF_CTRT_X;


--
CREATE VIEW CORE AS
SELECT CORE_S,
    INVENTORY_OBJ_ID as IDENTIFIER,
    ROCK_MATERIAL_S,
    ROCK_MATERIAL_T,
    R_EXISTENCE_KD_NM as REF_EXISTENCE_KIND,
    WELLBORE_S
FROM SLEGGE_EPI.CORE;


--
CREATE VIEW FACILITY_CLASSIFICATION AS
SELECT FACILITY_CLSN_S as FACILITY_CLASSIFICATION_S,
    CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    FACILITY_S,
    FACILITY_T,
    FACILITY_CLASS_S,
    FCL_CLASS_NAME as FACILITY_CLASS_NAME
FROM SLEGGE_EPI.FACILITY_CLSN;


--
CREATE VIEW FACILITY_COMPOSITION AS
SELECT FACILITY_CMPS_S as FACILITY_COMPOSITION_S,
    ENTITY_TYPE_NM as ENTITY_TYPE_NAME,
    PART_S,
    PART_T,
    WHOLE_S,
    WHOLE_T
FROM SLEGGE_EPI.FACILITY_CMPS;


--
CREATE VIEW FIELD AS
SELECT FIELD_S,
    IDENTIFIER,
    R_NAMING_SYSTEM_KD as REF_NAMING_SYSTEM,
    UNIQUE_MEMBER_OF_S
FROM SLEGGE_EPI.FIELD;


--
CREATE VIEW MATERIAL_CLASS AS
SELECT MATERIAL_CLASS_S,
    CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    CLSN_CLS_NAME as NAME
FROM SLEGGE_EPI.MATERIAL_CLASS;


--
CREATE VIEW MATERIAL_CLASSIFICATION AS
SELECT MATERIAL_S,
    MATERIAL_CLASS_S
FROM SLEGGE_EPI.MATERIAL_CLSN;


--
CREATE VIEW COMPONENT_MATERIAL AS
SELECT ENTITY_TYPE_NM as ENTITY_TYPE_NAME,
    CHARACTERIZE_S,
    INCORPORATE_S
FROM SLEGGE_EPI.MATERIAL_CMPS;


--
CREATE VIEW PROCESS_PARAMETER AS
SELECT PROCESS_PAR_S PROCESS_PARAMETER_S,
    PROCESS_PAR_NAME NAME,
    PB_PARAMETER_CODE,
    TYPICAL_ACT_S TYPICAL_ACTIVITY_S
FROM SLEGGE_EPI.PROCESS_PAR;


--
CREATE VIEW INTERPROCESS_DATA AS
SELECT NTRPROC_DATA_S as INTERPROCESS_DATA_S,
    ACTIVITY_S,
    PROCESS_PAR_S as PROCESS_PARAMETER_S,
    PROCESS_PAR_NAME as PROCESS_PARAMETER_NAME,
    REAL_VALUE
FROM SLEGGE_EPI.NTRPROC_DATA;


--
CREATE VIEW PTY_LOCATION_1D AS
SELECT DATA_VALUE_1_O,
    DATA_VALUE_1_OU,
    ACTIVITY_S,
    WELLBORE_POINT_S
FROM SLEGGE_EPI.P_LOCATION_1D;


--
CREATE VIEW PTY_LOCATION_2D AS
SELECT P_LOCATION_2D_S as PTY_LOCATION_2D_S,
    DATA_VALUE_1_O,
    DATA_VALUE_1_OU,
    DATA_VALUE_2_O,
    DATA_VALUE_2_OU,
    WELL_SURFACE_PT_S as WELL_SURFACE_POINT_S
FROM SLEGGE_EPI.P_LOCATION_2D;


--
CREATE VIEW PTY_PRESSURE AS
SELECT P_PRESSURE_S as PTY_PRESSURE_S,
    ACTIVITY_S,
    DATA_VALUE,
    DATA_VALUE_U
FROM SLEGGE_EPI.P_PRESSURE;


--
CREATE VIEW ROCK_FEATURE AS
SELECT ROCK_FEATURE_S,
    DESCRIPTION
FROM SLEGGE_EPI.ROCK_FEATURE;


--
CREATE VIEW TOPOLOGICAL_RELATIONSHIP AS
SELECT TOPOLOGICAL_REL_S as TOPOLOGICAL_RELATIONSHIP_S,
    BOUNDARY_OVERLAP,
    PRIM_TOPLG_OBJ_S as PRIMARY_TOPOLOGICAL_OBJECT_S,
    PRIM_TOPLG_OBJ_T as PRIMARY_TOPOLOGICAL_OBJECT_T,
    R_OBJECT_NTRS_KIND as REF_OBJECT_INTERSECTION,
    R_TOPOLOGIC_REL_KD as REF_TOPOLOGICAL_RELATIONSHIP,
    SEC_TOPLG_OBJ_S as SECONDARY_TOPOLOGICAL_OBJECT_S,
    SEC_TOPLG_OBJ_T as SECONDARY_TOPOLOGICAL_OBJECT_T
FROM SLEGGE_EPI.TOPOLOGICAL_REL;


--
CREATE VIEW OTHER_FACILITY AS
SELECT OTHER_FACILITY_S,
    IDENTIFYING_FCL_S as IDENTIFYING_FACILITY_S,
    IDENTIFYING_FCL_T as IDENTIFYING_FACILITY_T,
    GENERAL_FCL_NAME as NAME,
    R_EXISTENCE_KD_NM as REF_EXISTENCE_KIND,
    TYPICAL_FCL_NAME as TYPICAL_FACILITY,
    PARENT_FACILITY_S
FROM SLEGGE_EPI.OTHER_FACILITY;


--
CREATE VIEW WELL AS
SELECT WELL_S,
    WELL_ID as IDENTIFIER,
    SPUD_DATE,
    R_EXISTENCE_KD_NM as REF_EXISTENCE_KIND,
    TYPICAL_FCL_NAME as TYPICAL_FACILITY,
    R_NAMING_SYSTEM_KD as REF_NAMING_SYSTEM
FROM SLEGGE_EPI.WELL;


--
CREATE VIEW WELL_ALIAS AS
SELECT WELL_ALIAS_S,
    EFFECTIVE_DATE,
    EXPIRY_DATE,
    IDENTIFIER,
    R_NAMING_SYSTEM_KD as REF_NAMING_SYSTEM,
    WELL_S,
    WELL_ID as WELL_IDENTIFIER
FROM SLEGGE_EPI.WELL_ALIAS;


--
CREATE VIEW WELL_SURFACE_POINT AS
SELECT WELL_SURFACE_PT_S as WELL_SURFACE_POINT_S,
    WELL_SURFACE_PT_ID as IDENTIFIER,
    WATER_DEPTH,
    WATER_DEPTH_U,
    WELL_S
FROM SLEGGE_EPI.WELL_SURFACE_PT;


--
CREATE VIEW WELLBORE AS
SELECT WELLBORE_S,
    COMPLETION_DATE,
    WELLBORE_ID as IDENTIFIER,
    R_EXISTENCE_KD_NM as REF_EXISTENCE_KIND,
    WELL_ID WELL_IDENTIFIER,
    CURRENT_TRACK,
    WELL_S
FROM SLEGGE_EPI.WELLBORE
WHERE SECURITY_LABELS_S = 0;


--
CREATE VIEW WELLBORE_INTERVAL AS
SELECT WELLBORE_INTV_S as WELLBORE_INTERVAL_S,
    ACTIVITY_S,
    BOTTOM_DEPTH,
    BOTTOM_DEPTH_U,
    EMODEL_OBJECT_ID as IDENTIFIER,
    R_WELLBORE_INTV_NM as REF_WELLBORE_INTERVAL,
    TOP_DEPTH,
    TOP_DEPTH_U,
    WELL_COMPLETION_S,
    WELL_ID as WELL_IDENTIFIER,
    WELLBORE_S,
    WB_CMPN_FCL_S as WELLBORE_COMPONENT_FACILITY_S,
    WB_CMPN_FCL_T as WELLBORE_COMPONENT_FACILITY_T,
    WELLBORE_ID as WELLBORE_IDENTIFIER
FROM SLEGGE_EPI.WELLBORE_INTV;


--
CREATE VIEW UNCERTAINTY_CLASS AS
SELECT UNCERTAINTY_CLS_S as UNCERTAINTY_CLASS_S,
    CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    CLSN_CLS_NAME as NAME
FROM SLEGGE_EPI.UNCERTAINTY_CLS;


--
CREATE VIEW POSITION_UNCERTAINTY_CLSN AS
SELECT POS_UNCR_CLSN_S as POSITION_UNCERTAINTY_CLSN_S,
    CLASSIFICATION_SYS as CLASSIFICATION_SYSTEM,
    EMODEL_OBJECT_S as EARTH_MODEL_OBJECT_S,
    EMODEL_OBJECT_T as EARTH_MODEL_OBJECT_T,
    UNCERTAINTY_CLS_S as UNCERTAINTY_CLASS_S,
    UNCR_CLASS_NAME as UNCERTAINTY_CLASS_NAME
FROM SLEGGE_EPI.POS_UNCR_CLSN;


--
CREATE VIEW PTY_GEOMETRY_3D_EDGE AS
SELECT P_GEOM_3D_EDG_S as PTY_GEOMETRY_3D_EDGE_S,
    ACTIVITY_S,
    EDGE_S,
    EDGE_T
FROM SLEGGE_EPI.P_GEOM_3D_EDG;


----------------------------------
--- VIEWS FOR FRAME_LEAF
----------------------------------


--
CREATE VIEW SUPP_DATA_TYPE AS
SELECT SK,
    OWNER_SK
FROM SLEGGE_EPI.SUPP_DATA_TYPE;


--
CREATE VIEW DAE_DIM AS
SELECT FRAME_SK
FROM SLEGGE_EPI.DAE_DIM;


--
CREATE VIEW DAE_LEAF AS
SELECT FRAME_SK,
    REC_KEY,
    LEAF_ORDINAL
FROM SLEGGE_EPI.DAE_LEAF;


--
CREATE VIEW BLOB_HEADER AS
SELECT RECOG_KEY,
    DATA_KEY
FROM SLEGGE_EPI.BLOB_HEADER;


--
CREATE VIEW BLOB_TABLE AS
SELECT DATA_KEY,
    BLOB
FROM SLEGGE_EPI.BLOB_TABLE;




-----------------------------------
--- DOCUMENTS: TABLE AND VIEW
-----------------------------------


---
CREATE TABLE "DENORMALIZED_DOCUMENTS_" (
   DENORMALIZED_DOCUMENTS_S	VARCHAR2(19) NOT NULL,
   REPORT_IDENTIFIER 				VARCHAR2(40) NOT NULL,
   ARCHIVE_OBJECT_TYPE_S 		VARCHAR2(19) NOT NULL,
   ITEM_TYPE_S 					VARCHAR2(19) NOT NULL,
   REFERENCE_NAME 				VARCHAR2(40),
   TITLE 							VARCHAR2(255),
   COMPANY_S 					VARCHAR2(19) NOT NULL,
   COMMENTS 						VARCHAR2(4000),
   FILE_NAME 						VARCHAR2(256),
   STORAGE_LOCATION_S 			VARCHAR2(19),
   STORAGE_LOCATION_INDEX 		VARCHAR2(40),
   SECURITY_LABEL_S				NUMBER,
--
  CONSTRAINT DENORMALIZED_DOCUMENTS_PK PRIMARY KEY (DENORMALIZED_DOCUMENTS_S),
--
  CONSTRAINT COMPANY_C FOREIGN KEY (COMPANY_S) REFERENCES "SLEGGE_EPI"."BSASC" (BSASC_S),
  CONSTRAINT ARCHIVE_OBJECT_TYPE_C FOREIGN KEY (ARCHIVE_OBJECT_TYPE_S) REFERENCES "SLEGGE_EPI"."DOC_SPEC_CLS" (DOC_SPEC_CLS_S),
  CONSTRAINT STORAGE_LOCATION_C FOREIGN KEY (STORAGE_LOCATION_S) REFERENCES "SLEGGE_EPI"."OTHER_FACILITY" (OTHER_FACILITY_S)  
);



-- 
CREATE VIEW "DENORMALIZED_DOCUMENTS" AS
SELECT  DENORMALIZED_DOCUMENTS_S,
     REPORT_IDENTIFIER,
     ARCHIVE_OBJECT_TYPE_S,
     ITEM_TYPE_S,
     REFERENCE_NAME,
     TITLE,
     COMMENTS,
     FILE_NAME,
     STORAGE_LOCATION_INDEX
FROM "DENORMALIZED_DOCUMENTS_"
WHERE SECURITY_LABEL_S = 0;





-----------------------------------
--- FUNCTIONS
-----------------------------------


CREATE FUNCTION FixZoneName(zname VARCHAR2) RETURN VARCHAR2 AS 
  str VARCHAR2(255);                                                            
BEGIN                                                                           
  str := zname;                                                                 
  str := REPLACE(str,' Top','');                                                
  str := REPLACE(str,' Base','');                                                                                                   
  str := LTrim(RTrim(str));                                                                                 
  Return(str);                                                                  
END FixZoneName;
/

--
CREATE FUNCTION FrameMDTVD(dataKey_tvd NUMBER, dataKey_md NUMBER, md NUMBER)
RETURN NUMBER AS 
LANGUAGE JAVA NAME
'SlgBlobSQLSimple.MDTVD(java.lang.Long,java.lang.Long,java.lang.Double) return java.lang.Double';
/

--
CREATE PACKAGE FRAME_LEAF AS
FUNCTION MDTVD (wn IN VARCHAR2, md IN NUMBER) RETURN NUMBER;
FUNCTION GetLeafID (wn IN VARCHAR2, frame_leaf_tp IN VARCHAR2) RETURN NUMBER;
END FRAME_LEAF;
/


CREATE PACKAGE BODY FRAME_LEAF AS
FUNCTION MDTVD (wn IN VARCHAR2, md IN NUMBER) RETURN NUMBER AS
    tvd_blk_id NUMBER;
    md_blk_id NUMBER;
    cnt NUMBER;

BEGIN
    SELECT COUNT(*) INTO cnt 
    FROM SLEGGE.WELLBORE
    WHERE 
         IDENTIFIER = wn AND 
         REF_EXISTENCE_KIND = 'actual';

    IF (cnt > 0) THEN
      md_blk_id := GetLeafID(wn, 'measured_depth');
      IF (md_blk_id = -1) THEN
        Return -99001;
      END IF;
      tvd_blk_id := GetLeafID (wn, 'true_vertical_depth');
      IF (tvd_blk_id = -1) THEN
        Return -99002;
      END IF;

      Return Round(FrameMDTVD(tvd_blk_id, md_blk_id, md), 2);
    ELSE
      Return -99000;  /* well does not exist */ 
    END IF;
END MDTVD;
  
  
--  
FUNCTION GetLeafID (wn IN VARCHAR2, frame_leaf_tp IN VARCHAR2) RETURN NUMBER AS
    blob_data_key NUMBER;
    cnt NUMBER;

BEGIN
    SELECT COUNT(*) INTO cnt 
    FROM
      SLEGGE.activity_class r97596915,
      SLEGGE.activity_class r334516808,
      SLEGGE.wellbore_interval a619812793,
      SLEGGE.activity a791101285,
      SLEGGE.activity r866145957,
      SLEGGE.wellbore r1090916117,
      SLEGGE.well r1520541859,
      SLEGGE.pty_geometry_3d_edge a2032725149,
      SLEGGE.BLOB_TABLE a,
      SLEGGE.BLOB_HEADER b,
      SLEGGE.DAE_LEAF c,
      SLEGGE.DAE_DIM d,
      SLEGGE.SUPP_DATA_TYPE e
    WHERE
      a.data_key = b.data_key AND
      b.recog_key = c.rec_key AND
      c.FRAME_SK = d.FRAME_SK AND
      d.FRAME_SK = e.SK AND
      e.OWNER_SK = a2032725149.pty_geometry_3d_edge_s AND
      r97596915.name = 'wellbore path' AND
      r334516808.classification_system = 'activity subtypes' AND
      r334516808.name = 'wellbore directional survey' AND
      a619812793.wellbore_s = r1090916117.wellbore_s AND
      a619812793.activity_s = a791101285.activity_s AND
      (a619812793.well_identifier = r1520541859.identifier OR 
              (a619812793.well_identifier IS NULL AND r1520541859.identifier IS NULL)) AND
      a619812793.ref_wellbore_interval='wellbore path edge' AND
      a791101285.kind_s = r97596915.activity_class_s AND
      a791101285.containing_activity_s = r866145957.activity_s AND
      r866145957.kind_s = r334516808.activity_class_s AND
      r866145957.facility_s = r1090916117.wellbore_s AND
      r1090916117.well_s = r1520541859.well_s AND
      r1090916117.ref_existence_kind = 'actual' AND
      a2032725149.activity_s = a791101285.activity_s AND
      a2032725149.edge_s = a619812793.wellbore_interval_s AND
      r1090916117.identifier = wn AND
      DECODE(c.LEAF_ORDINAL,
            '3','true_vertical_depth',
            '4','measured_depth',
            '11','delta_easting',
            '12','delta_northing', c.LEAF_ORDINAL) = frame_leaf_tp; 
       
    IF (cnt > 0) THEN
    SELECT a.data_key INTO blob_data_key
    FROM
      SLEGGE.activity_class r97596915,
      SLEGGE.activity_class r334516808,
      SLEGGE.wellbore_interval a619812793,
      SLEGGE.activity a791101285,
      SLEGGE.activity r866145957,
      SLEGGE.wellbore r1090916117,
      SLEGGE.well r1520541859,
      SLEGGE.pty_geometry_3d_edge a2032725149,
      SLEGGE.BLOB_TABLE a,
      SLEGGE.BLOB_HEADER b,
      SLEGGE.DAE_LEAF c,
      SLEGGE.DAE_DIM d,
      SLEGGE.SUPP_DATA_TYPE e
    WHERE
      a.data_key = b.data_key AND
      b.recog_key = c.rec_key AND
      c.FRAME_SK = d.FRAME_SK AND
      d.FRAME_SK = e.SK AND
      e.OWNER_SK = a2032725149.pty_geometry_3d_edge_s AND
      r97596915.name = 'wellbore path' AND
      r334516808.classification_system = 'activity subtypes' AND
      r334516808.name = 'wellbore directional survey' AND
      a619812793.wellbore_s = r1090916117.wellbore_s AND
      a619812793.activity_s = a791101285.activity_s AND
      (a619812793.well_identifier = r1520541859.identifier OR 
            (a619812793.well_identifier IS NULL AND r1520541859.identifier IS NULL)) AND
      a619812793.ref_wellbore_interval = 'wellbore path edge' AND
      a791101285.kind_s = r97596915.activity_class_s AND
      a791101285.containing_activity_s = r866145957.activity_s AND
      r866145957.kind_s = r334516808.activity_class_s AND
      r866145957.facility_s = r1090916117.wellbore_s AND
      r1090916117.well_s = r1520541859.well_s AND
      r1090916117.ref_existence_kind = 'actual' AND
      a2032725149.activity_s = a791101285.activity_s AND
      a2032725149.edge_s = a619812793.wellbore_interval_s AND
      r1090916117.identifier = wn AND
      DECODE(c.LEAF_ORDINAL,
            '3','true_vertical_depth',
            '4','measured_depth',
            '11','delta_easting',
            '12','delta_northing', c.LEAF_ORDINAL) = frame_leaf_tp AND 
      ROWNUM < 2;

      RETURN blob_data_key;
    ELSE
      Return -1;
    END IF;
END GetLeafID;
  
END FRAME_LEAF;
/





-----------------------------------
--- MATERIALIZED VIEWS
-----------------------------------


CREATE MATERIALIZED VIEW "STRATIGRAPHIC_HIERARCHY"
  REFRESH
  START WITH SysDate+0.55
  NEXT SysDate + 1 AS
SELECT 
    data_collection_2.name strat_column_identifier,
    rock_feature_1.description strat_unit_identifier,
    rock_feature_2.description parent_strat_zone_id
FROM
    rock_feature rock_feature_1,
    material_class,
    component_material,
    data_collection_content data_collection_content_1,
    pty_quantity,
    ref_property_kind,
    data_collection_content data_collection_content_2,
    material_classification,
    reference_data_collection,
    ref_classification_system,
    ref_descriptive_property,
    rock_feature rock_feature_2,
    classification_system,
    data_collection data_collection_1,
    data_collection data_collection_2
WHERE
    material_class.classification_system=classification_system.name AND
    component_material.characterize_s=rock_feature_2.rock_feature_s AND
    component_material.incorporate_s=rock_feature_1.rock_feature_s AND
    component_material.entity_type_name='COMPONENT_MATERIAL' AND
    data_collection_content_1.collection_part_s=rock_feature_2.rock_feature_s AND
    data_collection_content_1.part_of_s=data_collection_2.data_collection_s AND
    pty_quantity.material_s=rock_feature_1.rock_feature_s AND
    pty_quantity.ref_property_kind_s=ref_property_kind.ref_property_kind_s AND
    pty_quantity.entity_type_name='PTY_QUANTITY' AND
    pty_quantity.quantity_value_q='unitless' AND
    pty_quantity.quantity_value_u='unitless' AND
    ref_property_kind.ref_property_type_s=ref_descriptive_property.ref_descriptive_property_s AND
    data_collection_content_2.part_of_s=data_collection_2.data_collection_s AND
    data_collection_content_2.collection_part_s=rock_feature_1.rock_feature_s AND
    material_classification.material_s=rock_feature_1.rock_feature_s AND
    material_classification.material_class_s=material_class.material_class_s AND
    reference_data_collection.ref_data_s=ref_classification_system.ref_classification_system_s AND
    reference_data_collection.in_data_collection_s=data_collection_1.data_collection_s AND
    ref_descriptive_property.name='sequence number' AND
    classification_system.kind=ref_classification_system.name AND
    data_collection_1.name='stratigraphic hierarchy types' AND
    data_collection_2.ref_data_collection_type IN ('stratigraphic hierarchy');


--
CREATE MATERIALIZED VIEW "STRATIGRAPHIC_ZONE"
  REFRESH WITH PRIMARY KEY
  START WITH SysDate+0.5
  NEXT SysDate + 1 AS
SELECT 
    wellbore.identifier wellbore,
    data_collection.name strat_column_identifier,
    SubStr(wellbore_point_2.identifier,Instr(wellbore_point_2.identifier,' ',-1,1)+1) strat_interp_version,
    FixZoneName(stratigraphic_marker.description) strat_unit_identifier,
    FixZoneName(tops_1.description) strat_zone_identifier,
    min(depths.data_value_1_o) strat_zone_entry_md,
    max(depths.data_value_1_o) strat_zone_exit_md,
    depths.data_value_1_ou strat_zone_depth_uom
FROM
    earth_model,
    data_collection,
    data_collection_content,
    wellbore_point wellbore_point_2,
    feature_boundary_part,
    wellbore_point wellbore_point_1,
    boundary_classification tops_1,
    wellbore,
    earth_model_composition earth_model_composition_2,
    pty_location_1d depths,
    stratigraphic_marker,
    earth_model_composition earth_model_composition_1
WHERE
    data_collection.ref_data_collection_type='stratigraphic hierarchy' AND
    data_collection_content.part_of_s=data_collection.data_collection_s AND
    data_collection_content.collection_part_s=earth_model.earth_model_s AND
    earth_model.earth_model_s= earth_model_composition_1.earth_model_s AND
    wellbore_point_2.wellbore_s=wellbore.wellbore_s AND
    wellbore_point_2.ref_wellbore_point='artificial stratigraphy' AND
    feature_boundary_part.feature_boundary_s=stratigraphic_marker.stratigraphic_marker_s AND
    wellbore_point_1.geologic_feature_s=feature_boundary_part.feature_boundary_part_s AND
    wellbore_point_1.wellbore_s=wellbore.wellbore_s AND
    wellbore_point_1.ref_wellbore_point='wellbore stratigraphic pick' AND
    wellbore_point_1.azimuth_u='dega' AND
    wellbore_point_1.inclination_from_vertical_u='dega' AND
    tops_1.feature_boundary_s=feature_boundary_part.feature_boundary_part_s AND
    tops_1.classification_system='structural discontinuity type' AND
    wellbore.ref_existence_kind='actual' AND
    earth_model_composition_2.earth_model_s=earth_model_composition_1.earth_model_s AND
    earth_model_composition_2.spatial_object_s=wellbore_point_2.wellbore_point_s AND
    depths.wellbore_point_s=wellbore_point_1.wellbore_point_s AND
    stratigraphic_marker.entity_type_name='STRATIGRAPHIC_MARKER' AND
    earth_model_composition_1.spatial_object_s=wellbore_point_1.wellbore_point_s
GROUP BY
  wellbore.identifier,
  data_collection.name,
  wellbore_point_2.identifier,
  FixZoneName(stratigraphic_marker.description),
  FixZoneName(tops_1.description),
  depths.data_value_1_ou;


--
CREATE MATERIALIZED VIEW "STRAT_TOP" 
  REFRESH WITH PRIMARY KEY
  START WITH SysDate+0.6
  NEXT SysDate + 1 AS
SELECT 
  stratigraphic_zone.wellbore,
  stratigraphic_zone.strat_column_identifier,
  stratigraphic_zone.strat_interp_version,
  stratigraphic_zone.strat_unit_identifier,
  stratigraphic_zone.strat_zone_identifier,
  stratigraphic_zone.strat_zone_entry_md,
  stratigraphic_zone.strat_zone_exit_md,
  stratigraphic_zone.strat_zone_depth_uom
FROM
  stratigraphic_zone,
  stratigraphic_hierarchy
WHERE 
  stratigraphic_zone.strat_column_identifier = stratigraphic_hierarchy.strat_column_identifier AND
  stratigraphic_zone.strat_unit_identifier = stratigraphic_hierarchy.strat_unit_identifier AND
  stratigraphic_zone.strat_zone_entry_md != stratigraphic_zone.strat_zone_exit_md;


--
CREATE MATERIALIZED VIEW "STRAT_LEVEL_PARENTS" 
  REFRESH WITH PRIMARY KEY
  START WITH SysDate+0.65
  NEXT SysDate + 1 AS
SELECT
  strat_column_identifier,
  strat_unit_identifier
FROM
  stratigraphic_hierarchy;


--
CREATE MATERIALIZED VIEW "PICKED_STRATIGRAPHIC_ZONES"
  REFRESH WITH PRIMARY KEY
  START WITH SysDate+0.7
  NEXT SysDate + 1 AS
SELECT
  wellbore,
  strat_top.strat_column_identifier,
  strat_interp_version,
  strat_zone_identifier,
  strat_zone_entry_md,
  strat_zone_exit_md,
  strat_zone_depth_uom
FROM 
  strat_top,
  strat_level_parents
WHERE
  strat_top.strat_column_identifier = strat_level_parents.strat_column_identifier AND
  strat_top.strat_unit_identifier = strat_level_parents.strat_unit_identifier;


