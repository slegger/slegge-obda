import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.sql.*;

public class SlgBlobSQLSimple {

    private static final int DOUBLE_BYTES = Double.SIZE / Byte.SIZE;
    private static final Double ERROR_VALUE = new Double(0);

    public static Double MDTVD(Long dataKey_tvd, Long dataKey_md, Double md) throws SQLException {

        Connection conn = DriverManager.getConnection("jdbc:default:connection:");

        try (PreparedStatement pstmt = conn.prepareStatement(
                "SELECT BLOB FROM BLOB_TABLE WHERE DATA_KEY = ?")) {

            int ind = 0;
            double md1 = 0, md2 = 0;
            pstmt.setLong(1, dataKey_md.longValue());
            try (ResultSet rs_md = pstmt.executeQuery()) {
                rs_md.next();
                byte[] buffer_md = rs_md.getBytes("BLOB");
                DataInputStream in = new DataInputStream (new ByteArrayInputStream(buffer_md));
                final int md_len = buffer_md.length / DOUBLE_BYTES;

                for (int i = 0; i < md_len && md2 < md; i++) {
                    md1 = md2;
                    md2 = in.readDouble();
                    ind = i;
                }
            }
            catch (Exception e) {
                System.err.println("Unable to read MD doubles:" + e);
                return ERROR_VALUE;
            }

            double tvd1 = 0, tvd2 = 0;
            pstmt.setLong(1, dataKey_tvd.longValue());
            try (ResultSet rs_tvd = pstmt.executeQuery()) {
                rs_tvd.next();
                byte[] buffer_tvd = rs_tvd.getBytes("BLOB");
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(buffer_tvd));

                for (int i = 0; i < ind; i++)
                    tvd1 = in.readDouble();

                tvd2 = in.readDouble();
            }
            catch (Exception e) {
                System.err.println("Unable to read TVD doubles: " + e);
                return ERROR_VALUE;
            }

            return (md2 == md1) ? (tvd1 + tvd2) / 2
                    : tvd1 + (tvd2 - tvd1) * (md - md1) / (md2 - md1);
        }
        catch (Exception e) {
            System.err.println("MDTVD exception: " + e);
        }

        return ERROR_VALUE;
    }
}
