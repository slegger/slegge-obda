


ONTOLOGYNAME=subsurface-exploration
MAPPINGNAME  = slegge
URIROOT  = http://slegger.gitlab.io/slegge-obda/


##########
# Ontology

ontology/$(ONTOLOGYNAME).owl: ontology/$(ONTOLOGYNAME).ttl
	rapper -i turtle -o rdfxml-abbrev $< > $@ 

ontology/$(ONTOLOGYNAME): ontology/$(ONTOLOGYNAME).owl
	cp $^ $@

ontology/index.html:
	wget http://www.essepuntato.it/lode/reasoner/$(URIROOT)ontology/$(ONTOLOGYNAME) -O $@
	sed -i -r 's/Table of Content/Table of Contents/g' $@      # TYPO
	chmod 644 $@

# sed -i -r 's/Machester/Manchester/g' $@		    # TYPO
# #sed -i -r 's/(<title>[^<]*<\/title>){2}/\1/g' $@	   # INVALID
# #sed -i -r 's/(<script)/\1 type="text\/javascript"/g' $@    # INVALID
# #sed -i -r 's/href="[^"]*rec\.css"( rel="stylesheet")/href="http:\/\/sws.ifi.uio.no\/sws.css"\1/g' $@  # replace css
# #sed -i -r 's/<link rel="shortcut icon"[^>]*>//g' $@	# REMOVE icon
# #sed -i -r 's/<a href="#([^"]+)" title="([^#]+#([^"]+))">/<a href="#\3" title="\2">/g' $@  # replace links
# sed -i -r 's/\*([^*]+)\*/<b>\1<\/b>/g' $@		  # *bold*
# #sed -i -r 's/!([^!]+)!/<i>\1<\/i>/g' $@		    # !italic!
# #sed -i -r 's/\[\[(#?([^\[]+))\]\]/<a href="\1">\2<\/a>/g' $@   # [[link]]

##########
# Mappings

mappings/$(MAPPINGNAME): mappings/$(MAPPINGNAME).r2rml
	cp $^ $@

##########
# Queries


##########

.PHONY: all public

all: \
	ontology/$(ONTOLOGYNAME).owl \
	ontology/$(ONTOLOGYNAME) \
	ontology/index.html \
	mappings/$(MAPPINGNAME)

public: \
	ontology \
	mappings \
	query 
	mkdir -p $@
	cp -uR $^ $@
