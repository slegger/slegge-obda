PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w   a                     :Wellbore ;
         :name                 ?wellbore_name ;
         :hasWellboreInterval  ?wi .
    ?c   a                     :Core ;
         :extractedFrom        ?wi .
    ?wi  :overlapsWellboreInterval  ?z .
    ?z   :hasUnit              ?u .
    ?u   :name                 ?uName
    FILTER regex(?uName, "(Brent)|(Vestland)")
    ?c        :hasCoreSample        ?s .
    ?s        :hasPermeability  ?p .
    ?p        :valueInOriginalUnit  ?permeability .
    ?s        :hasDepth  ?sample_depth_tvd .
    ?sample_depth_tvd
              a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?sample_depth_tvd_m
    FILTER ( ?sample_depth_tvd_m > 3500 )
  }
