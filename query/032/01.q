PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w   a                     :Wellbore ;
         :name                 ?wellbore_name ;
         :hasWellboreInterval  ?wi .
    ?c   a                     :Core ;
         :extractedFrom        ?wi .
    ?wi  :overlapsWellboreInterval  ?z .
    ?z   :hasUnit              ?u .
    ?u   :name                 ?uName
    FILTER regex(?uName, "NORDLAND")
    ?c  :hasCoreSample        ?s .
    ?s  :hasPermeability  ?p .
    ?p  :valueInOriginalUnit  ?permeability
  }
