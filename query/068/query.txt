Average effective porosity values of Etive Fm.

##DOMAIN: Porosity, stratigraphy
##COMMENT: All wells with effective porosity logs in the Etive Fm. (between two well markers)
        Average porosity if any data source already has it OR, we calculate this in Petrel using porosity logs in loaded into Petrel and the well top markers.
