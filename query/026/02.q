PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w  a                     :Wellbore ;
        :name                 ?wName
    FILTER NOT EXISTS { ?w  :hasWellboreInterval  ?z .
                        ?z  :hasUnit              ?u .
                        ?u  :name                 ?strat_unit_name
                        FILTER regex(?strat_unit_name, "Tarbert")
                      }
    ?well  :hasWellbore  ?w ;
           :locatedIn    ?pos .
    ?pos   :latitude     ?lat ;
           :longitude    ?long
  }
