PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wName ;
           :hasWellboreInterval  ?z .
    ?z     :hasUnit              ?u .
    ?u     :name                 ?strat_unit_name .
    ?z     :hasTopDepth  ?top .
    ?top   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?top_depth_md_m .
    ?z     :hasBottomDepth  ?bot .
    ?bot   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?bot_depth_md_m .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    OPTIONAL
      { ?z        :hasTopDepth  ?top_tvd .
        ?top_tvd  a                     :TrueVerticalDepth ;
                  :valueInStandardUnit  ?top_depth_tvd_m .
        ?z        :hasBottomDepth  ?bot_tvd .
        ?bot_tvd  a                     :TrueVerticalDepth ;
                  :valueInStandardUnit  ?bot_depth_tvd_m
      }
  }
