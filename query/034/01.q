PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w  a                     :Well ;
        :hasWellbore          ?wlb
    FILTER NOT EXISTS { ?wlb   :hasWellboreInterval  ?int .
                        ?core  :extractedFrom        ?int
                      }
  }
