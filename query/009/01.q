PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasFormationPressure  ?pressure .
    ?pressure  a                    :Pressure ;
              :hasDepth  ?pressure_depth
    OPTIONAL
      { ?pressure_depth
                  :inWellboreInterval  ?strat_zone .
        ?strat_zone  :hasUnit          ?strat_unit
      }
  }
