PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name ?sample_name ?uName ?permeability
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore_name ;
              :hasWellboreInterval  ?interval .
    ?c        :extractedFrom        ?interval ;
              a                     :Core .
    ?interval  :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?u .
    ?u        :name                 ?uName
    FILTER regex(?uName, "BRENT", "i")
    ?c  :hasCoreSample        ?s .
    ?s  :hasPermeability  ?p ;
        :name                 ?sample_name .
    ?p  :valueInStandardUnit  ?permeability
    FILTER ( ?permeability > 1.0 )
  }
