PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name ?sample_name ?uName ?permeability ?top_md_m ?bot_md_m ?top_tvd_m ?bot_tvd_m
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore_name ;
              :hasWellboreInterval  ?interval .
    ?c        :extractedFrom        ?interval ;
              a                     :Core .
    ?interval  :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?u .
    ?u        :name                 ?uName
    FILTER regex(?uName, "NORDLAND")
    ?c  :hasCoreSample        ?s .
    ?s  :hasPermeability  ?p ;
        :name                 ?sample_name .
    ?p  :valueInStandardUnit  ?permeability
    OPTIONAL
      { ?interval  :hasTopDepth  ?top_md .
        ?top_md   a                     :MeasuredDepth ;
                  :valueInStandardUnit  ?top_md_m
      }
    OPTIONAL
      { ?interval  :hasBottomDepth  ?bot_md .
        ?bot_md   a                     :MeasuredDepth ;
                  :valueInStandardUnit  ?bot_md_m
      }
    OPTIONAL
      { ?interval  :hasTopDepth  ?top_tvd .
        ?top_tvd  a                     :TrueVerticalDepth ;
                  :valueInStandardUnit  ?top_tvd_m
      }
    OPTIONAL
      { ?interval  :hasBottomDepth  ?bot_tvd .
        ?bot_tvd  a                     :TrueVerticalDepth ;
                  :valueInStandardUnit  ?bot_tvd_m
      }
  }
