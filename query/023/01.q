PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wName ?sample ?porosity ?top_depth_md
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wName ;
              :hasWellboreInterval  ?z .
    ?z        :hasUnit              ?u .
    ?u        :name                 ?strat_unit_name .
    ?wellbore  :hasWellboreInterval  ?cored_int .
    ?c        :extractedFrom        ?cored_int ;
              :hasCoreSample        ?sample .
    ?sample   :hasDepth  ?sample_depth .
    ?sample_depth
              :inWellboreInterval   ?z ;
              :hasDepth  ?sample_depth_md ;
              :hasDepth  ?sample_depth_tvd .
    ?sample_depth_md
              a                     :MeasuredDepth ;
              :valueInStandardUnit  ?sample_depth_md_m .
    ?sample_depth_tvd
              a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?sample_depth_tvd_m .
    ?sample   :hasPorosity  ?p .
    ?p        :valueInStandardUnit  ?porosity .
    ?z        :hasTopDepth  ?top_md .
    ?top_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_depth_md .
    ?z        :hasTopDepth  ?bot_md .
    ?bot_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_depth_md .
    ?z        :hasTopDepth  ?top_tvd .
    ?top_tvd  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?top_depth_tvd .
    ?z        :hasBottomDepth  ?bot_tvd .
    ?bot_tvd  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?bot_depth_tvd
  }
