PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w  a                     :Wellbore ;
        :name                 ?wellbore_name
    FILTER regex(?wellbore_name, "NO 34")
    ?w  :hasWellboreInterval  ?ci .
    ?c  :extractedFrom        ?ci ;
        :hasCoreSample        ?s .
    ?s  :name                 ?core_sample_name ;
        :hasPermeability  ?p .
    ?p  :valueInStandardUnit  ?permeability
  }
