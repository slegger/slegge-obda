PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wName ?sample ?permeability ?top_depth_md ?lat ?long
WHERE
  { ?w       a                     :Wellbore ;
             :name                 ?wName ;
             :hasWellboreInterval  ?ci .
    ?c       :extractedFrom        ?ci .
    ?ci      :overlapsWellboreInterval  ?z .
    ?z       :hasUnit              ?u .
    ?u       :name                 ?strat_unit_name .
    ?c       :hasCoreSample        ?sample .
    ?sample  :hasPermeability  ?p .
    ?p       :valueInStandardUnit  ?permeability .
    ?ci      :hasTopDepth  ?top .
    ?top     a                     :MeasuredDepth ;
             :valueInStandardUnit  ?top_depth_md .
    ?well    :hasWellbore          ?w ;
             :locatedIn            ?pos .
    ?pos     :latitude             ?lat ;
             :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
