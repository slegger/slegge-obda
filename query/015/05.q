PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wName ?sample ?permeability ?top_depth_md ?compl_date
WHERE
  { ?w       a                     :Wellbore ;
             :name                 ?wName ;
             :hasWellboreInterval  ?ci .
    ?c       :extractedFrom        ?ci .
    ?ci      :overlapsWellboreInterval  ?z .
    ?z       :hasUnit              ?u .
    ?u       :name                 ?strat_unit_name .
    ?c       :hasCoreSample        ?sample .
    ?sample  :hasPermeability  ?p .
    ?p       :valueInStandardUnit  ?permeability
    FILTER ( ?permeability > 1.0 )
    FILTER regex(?strat_unit_name, "BRENT", "i")
    ?ci   :hasTopDepth  ?top .
    ?top  a                     :MeasuredDepth ;
          :valueInStandardUnit  ?top_depth_md
    OPTIONAL
      { ?w  :completionDate  ?compl_date }
  }
