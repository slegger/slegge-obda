PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasWellboreInterval  ?int .
    ?int      :hasLogCurve          ?l ;
              :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?strat_unit .
    ?strat_unit  :name              ?strat_unit_name
    FILTER regex(?strat_unit_name, "BRENT")
  }
