PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>
PREFIX  ns2:  <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>
PREFIX  ns1:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

SELECT DISTINCT  ?c1 ?c2 ?c3 ?c4
WHERE
  { ?c1  ns1:type              :WellboreInterval .
    ?c2  ns1:type              :LogCurve .
    ?c3  ns1:type              :WellboreInterval .
    ?c4  ns1:type              :StratigraphicUnit .
    ?c1  :hasLogCurve          ?c2 ;
         :overlapsWellboreInterval  ?c3 .
    ?c3  :hasUnit              ?c4 .
    ?c4  :name                 ?a1
    FILTER regex(?a1, "Shetland", "i")
  }
