PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasWellboreInterval  ?int .
    ?int      :hasLogCurve          ?l ;
              :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?strat_unit .
    ?strat_unit  :name              ?strat_unit_name .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
