PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?w ?c1
WHERE
  { ?w        a                     :Wellbore ;
              :hasWellboreInterval  ?int .
    ?int      a                     :StratigraphicZone ;
              :hasUnit              ?c1 .
    ?c1       :ofStratigraphicColumn  ?col .
    ?col      a                     :ChronoStratigraphicColumn .
    ?int      :overlapsWellboreInterval  ?litho_int .
    ?litho_int  :hasUnit            ?lu .
    ?lu       :ofStratigraphicColumn  ?l_col .
    ?l_col    a                     :LithoStratigraphicColumn .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
ORDER BY ?w ?c1
