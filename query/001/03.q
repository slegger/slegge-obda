PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?top_md ?bottom_md ?unit ?column
WHERE
  { ?w       a                     :Wellbore ;
             :hasWellboreInterval  ?int ;
             :name                 ?wellbore .
    ?int     a                     :StratigraphicZone ;
             :hasTopDepth  ?top ;
             :hasTopDepth  ?bottom ;
             :hasUnit              ?u .
    ?top     :valueInStandardUnit  ?top_md .
    ?bottom  :valueInStandardUnit  ?bottom_md .
    ?u       :ofStratigraphicColumn  ?col ;
             :name                 ?unit .
    ?col     :name                 ?column .
    ?well    :hasWellbore          ?w ;
             :locatedIn            ?pos .
    ?pos     :latitude             ?lat ;
             :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
