PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?w
WHERE
  { ?w     a                     :Wellbore .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
