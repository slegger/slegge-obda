PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?unit ?column ?lat ?long
WHERE
  { ?w     a                     :Wellbore ;
           :hasWellboreInterval  ?int ;
           :name                 ?wellbore .
    ?int   a                     :StratigraphicZone ;
           :hasUnit              ?u .
    ?u     :ofStratigraphicColumn  ?col ;
           :name                 ?unit .
    ?col   :name                 ?column .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
