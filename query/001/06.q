PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w     a                     :Wellbore ;
           :hasWellboreInterval  ?int .
    ?int   a                     :StratigraphicZone ;
           :hasUnit              ?c1 .
    ?c1    :ofStratigraphicColumn  ?col .
    ?col   a                     :ChronoStratigraphicColumn .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
    ?f  a                     :FluidZone .
    ?w  :hasWellboreInterval  ?f ;
        :name                 ?wellbore .
    ?f  :overlapsWellboreInterval  ?int ;
        :fluidZoneContent     ?content
  }
