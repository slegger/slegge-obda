PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasWellboreInterval  ?int .
    ?int      a                     :StratigraphicZone ;
              :hasUnit              ?c1 .
    ?w        :hasWellboreInterval  ?litho_zone .
    ?litho_zone  :hasUnit           ?litho_unit ;
              :overlapsWellboreInterval  ?int .
    ?w        :hasWellboreInterval  ?f ;
              :name                 ?wellbore .
    ?f        :overlapsWellboreInterval  ?int ;
              :fluidZoneContent     ?content .
    ?w        :hasWellboreInterval  ?other_hc .
    ?other_hc  :fluidZoneContent    ?content .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
