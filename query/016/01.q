PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?total_cored_length ?lat ?long
WHERE
  { ?w    a                     :Wellbore ;
          :name                 ?wellbore ;
          :hasTotalCoreLength       ?tcl .
    ?tcl  :valueInStandardUnit  ?total_cored_length
    FILTER ( ?total_cored_length > 30 )
    ?well  :hasWellbore  ?w ;
           :locatedIn    ?pos .
    ?pos   :latitude     ?lat ;
           :longitude    ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
