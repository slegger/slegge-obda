PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore ?total_cored_length
WHERE
  { ?w    a                     :Wellbore ;
          :name                 ?wellbore ;
          :hasTotalCoreLength       ?tcl .
    ?tcl  :valueInStandardUnit  ?total_cored_length
    FILTER ( ?total_cored_length > 30 )
  }
