PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore ?total_cored_length ?strat_unit_name ?top_md ?bot_md
WHERE
  { ?w    a                     :Wellbore ;
          :name                 ?wellbore ;
          :hasTotalCoreLength       ?tcl .
    ?tcl  :valueInStandardUnit  ?total_cored_length
    FILTER ( ?total_cored_length > 30 )
    OPTIONAL
      { ?w        :hasWellboreInterval  ?strat_zone .
        ?strat_zone  a                  :StratigraphicZone ;
                  :hasUnit              ?strat_unit ;
                  :hasTopDepth  ?top ;
                  :hasBottomDepth  ?bot .
        ?strat_unit  :name              ?strat_unit_name .
        ?top      a                     :MeasuredDepth ;
                  :valueInStandardUnit  ?top_md .
        ?bot      a                     :MeasuredDepth ;
                  :valueInStandardUnit  ?bot_md
      }
  }
