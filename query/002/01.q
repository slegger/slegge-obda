PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?w
WHERE
  { ?w  a                     :Wellbore
    FILTER NOT EXISTS { ?w        :hasWellboreInterval  ?wi .
                        ?wi       :hasUnit              ?c1 .
                        ?c1       :name                 "Jurassic" ;
                                  :ofStratigraphicColumn  ?chrono_col .
                        ?chrono_col  a                  :ChronoStratigraphicColumn
                      }
  }
