PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name ?core_sample_name ?permeability ?porosity ?top_depth_tvd_m
WHERE
  { ?w  a                     :Wellbore ;
        :name                 ?wellbore_name
    FILTER regex(?wellbore_name, "NO 34")
    ?w        :hasWellboreInterval  ?wi .
    ?c        :extractedFrom        ?wi .
    ?wi       :hasTopDepth  ?top_depth .
    ?top_depth  a                   :TrueVerticalDepth ;
              :valueInStandardUnit  ?top_depth_tvd_m
    OPTIONAL
      { ?c  :hasCoreSample        ?s .
        ?s  :name                 ?core_sample_name ;
            :hasPermeability  ?p .
        ?p  :valueInStandardUnit  ?permeability
      }
    OPTIONAL
      { ?c    :hasCoreSample        ?s2 .
        ?s    :hasPorosity  ?por .
        ?por  :valueInStandardUnit  ?porosity
      }
  }
