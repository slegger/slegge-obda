PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore_name ;
              :hasWellboreInterval  ?interval .
    ?c        :extractedFrom        ?interval ;
              a                     :Core .
    ?interval  :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?u .
    ?u        :name                 ?uName
    FILTER regex(?uName, "HUGIN", "i")
    ?interval  :hasTopDepth  ?top .
    ?top      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_depth_md
    FILTER ( ?top_depth_md > 3000 )
  }
