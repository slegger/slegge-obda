PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore_name ;
              :hasWellboreInterval  ?interval .
    ?c        :extractedFrom        ?interval ;
              a                     :Core .
    ?interval  :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?u .
    ?u        :name                 ?uName
    FILTER regex(?uName, "HUGIN", "i")
    ?interval  :hasTopDepth  ?top .
    ?top      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_depth_md
    FILTER ( ?top_depth_md > 3000 )
    OPTIONAL
      { ?interval  :overlapsWellboreInterval  ?strat_zone .
        ?strat_zone  :hasUnit           ?strat_unit ;
                  :hasTopDepth  ?zone_top ;
                  :hasBottomDepth  ?zone_bot .
        ?strat_unit  :name              ?strat_unit_name .
        ?zone_top  :valueInStandardUnit  ?zone_top_md_m ;
                  a                     :MeasuredDepth .
        ?zone_bot  :valueInStandardUnit  ?zone_bot_md_m ;
                  a                     :MeasuredDepth
      }
  }
ORDER BY ?c
