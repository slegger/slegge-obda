PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wName ?sample ?porosity ?top_depth_md ?bot_depth_md
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wName ;
              :hasWellboreInterval  ?z .
    ?z        :hasUnit              ?u .
    ?u        :name                 ?strat_unit_name .
    ?wellbore  :hasWellboreInterval  ?cored_int .
    ?c        :extractedFrom        ?cored_int ;
              :hasCoreSample        ?sample .
    ?sample   :hasDepth  ?sample_depth .
    ?sample_depth
              :inWellboreInterval   ?z .
    ?sample   :hasPorosity  ?p .
    ?p        :valueInStandardUnit  ?porosity .
    ?z        :hasTopDepth  ?top .
    ?top      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_depth_md .
    ?z        :hasBottomDepth  ?bot .
    ?bot      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?bot_depth_md
  }
