PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :completionDate       ?cmpl ;
              :hasTotalCoreLength       ?total_cored .
    ?total_cored  a                 :TotalCoreLength
    FILTER ( ?cmpl > "2008-12-31" )
    ?total_cored  :valueInStandardUnit  ?total_core_m
    FILTER ( ?total_core_m > 50 )
  }
