PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w     :name                 ?wellbore ;
           :hasWellboreInterval  ?zone .
    ?zone  :hasUnit              ?unit
    FILTER regex(?unit, "Draupne", "i")
    ?zone     :overlapsWellboreInterval  ?cored_int .
    ?draupne_core
              :extractedFrom        ?cored_int .
    ?zone     :overlapsWellboreInterval  ?logged_LFP_GR .
    ?logged_LFP_GR
              :hasLogCurve          ?LFP_GR_log .
    ?LFP_GR_log  :logCurveNameShort  ?LFP_GR_logname
    FILTER regex(?LFP_GR_logname, "LFP_GR")
    ?zone     :overlapsWellboreInterval  ?logged_LFP_PHIT .
    ?LFP_PHIT_log
              :logCurveNameShort    ?LFP_PHIT_logname
    FILTER regex(?LFP_PHIT_logname, "LFP_PHIT")
    ?logged_LFP_PHIT
              :hasLogCurve          ?LFP_PHIT_log .
    ?zone     :overlapsWellboreInterval  ?logged_LFP_DT .
    ?logged_LFP_DT
              :hasLogCurve          ?LFP_DT_log .
    ?LFP_DT_log  :logCurveNameShort  ?LFP_DT_logname
    FILTER regex(?LFP_DT_logname, "LFP_DT")
    ?cored_int  :hasTopDepth  ?top_md ;
              :hasTopDepth  ?top_tvd ;
              :hasBottomDepth  ?bot_md ;
              :hasBottomDepth  ?bot_tvd .
    ?top_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_md_m .
    ?bot_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?bot_md_m .
    ?top_tvd  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?top_tvd_m .
    ?bot_tvd  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?bot_tvd_m
  }
