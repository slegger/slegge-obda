PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?stratigraphic_unit (AVG(?pressure) as ?avg_pressure)
WHERE
  { ?w     a                     :Wellbore ;
           :hasWellboreInterval  ?wi ;
           :hasFormationPressure  ?fp .
    ?fp    :valueInStandardUnit  ?formation_pressure ;
    	   :hasDepth  ?fpd.
    ?fpd   :inWellboreInterval   ?wi.
    ?wi    a                     :StratigraphicZone ;
           :hasUnit              ?stratigraphic_unit .
} GROUP BY (?stratigraphic_unit)
