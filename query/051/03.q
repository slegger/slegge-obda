PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?wi ?formation_pressure ?lat ?long
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wellbore ;
           :hasWellboreInterval  ?wi ;
           :hasFormationPressure  ?fp .
    ?fp    :valueInStandardUnit  ?formation_pressure .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
