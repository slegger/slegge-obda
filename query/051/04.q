PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore ;
              :hasWellboreInterval  ?wi ;
              :hasFormationPressure  ?fp .
    ?fp       :valueInStandardUnit  ?formation_pressure ;
              :hasDepth  ?fp_depth .
    ?fp_depth  :inWellboreInterval  ?wi .
    ?wi       :hasUnit              ?unit ;
              :overlapsWellboreInterval  ?logged .
    ?logged   :hasLogCurve          ?curve
  }
