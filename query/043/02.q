PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name ?core_sample_name ?permeability
WHERE
  { ?w  a                     :Wellbore ;
        :name                 ?wellbore_name
    FILTER regex(?wellbore_name, "NO 34")
    ?w  :hasWellboreInterval  ?wi .
    ?c  :extractedFrom        ?wi ;
        :hasCoreSample        ?s .
    ?s  :name                 ?core_sample_name
    OPTIONAL
      { ?s  :hasPermeability  ?p .
        ?p  :valueInStandardUnit  ?permeability
      }
  }
