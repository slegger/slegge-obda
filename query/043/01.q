PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name ?sample_name ?uName ?permeability ?lat ?long
WHERE
  { ?w  a                     :Wellbore ;
        :name                 ?wellbore_name ;
        :hasWellboreInterval  ?interval
    FILTER regex(?wellbore_name, "NO 34")
    ?c        :extractedFrom        ?interval ;
              a                     :Core .
    ?interval  :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?u .
    ?u        :name                 ?uName .
    ?c        :hasCoreSample        ?s .
    ?s        :hasPermeability  ?p ;
              :name                 ?sample_name .
    ?p        :valueInStandardUnit  ?permeability .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
