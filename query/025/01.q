PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasWellboreInterval  ?z ;
              :name                 ?wellbore_name .
    ?z        :hasUnit              ?strat_unit .
    ?strat_unit  :name              ?strat_unit_name .
    ?w        :hasFormationPressure  ?p .
    ?p        :hasDepth  ?p_depth .
    ?p_depth  a                     :MeasuredDepth ;
              :inWellboreInterval   ?z ;
              :valueInStandardUnit  ?pressure_depth_md_m .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos ;
              :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
  }
