PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasWellboreInterval  ?z ;
              :name                 ?wellbore_name .
    ?z        :hasUnit              ?strat_unit .
    ?strat_unit  :name              ?strat_unit_name .
    ?t        a                     :Temperature ;
              :valueInStandardUnit  ?temp ;
              :hasDepth  ?t_depth .
    ?t_depth  :inWellboreInterval   ?z ;
              a                     :MeasuredDepth ;
              :valueInStandardUnit  ?temp_depth_md_m .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
  }
