PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        :hasGeochemicalMeasurement  ?measurement .
    ?measurement  :peakName         ?peakName ;
              :cgType               ?cgtype
    OPTIONAL
      { ?measurement  :peakAmount  ?p }
    OPTIONAL
      { ?measurement  :peakArea  ?area }
    OPTIONAL
      { ?measurement  :peakHeight  ?h }
  }
