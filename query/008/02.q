PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :hasGeochemicalMeasurement  ?measurement .
    ?measurement  :cgType           ?cgtype ;
              :peakName             ?peakType ;
              :peakHeight           ?peak_height ;
              :peakAmount           ?peak_amount
  }
