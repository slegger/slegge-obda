PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w    a                     :Wellbore ;
          :wellboreDocument     ?doc .
    ?doc  :hasURL               ?document_hyperlink
  }
