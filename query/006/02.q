PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w    a                     :Wellbore ;
          :wellboreDocument     ?doc .
    ?doc  :hasURL               ?document_hyperlink
  }
ORDER BY ?w
