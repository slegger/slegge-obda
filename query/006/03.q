PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w     a                     :Wellbore ;
           :wellboreDocument     ?doc .
    ?doc   :hasURL               ?document_hyperlink .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
ORDER BY ?w
