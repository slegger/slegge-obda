PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?c1 ?c2 ?c8
WHERE
  { ?c1  a                     :Wellbore .
    ?c2  a                     :WellboreInterval .
    ?c8  a                     :WellboreInterval .
    ?c1  :hasWellboreInterval  ?c2 .
    ?c2  :overlapsWellboreInterval  ?c8
  }
LIMIT   100
