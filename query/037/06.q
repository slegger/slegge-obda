PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?c1 ?c2 ?c3 ?c8 ?c4 ?c6 ?c9 ?a3 ?c5 ?a1 ?c7 ?a2 ?c10
WHERE
  { ?c1   a                     :Wellbore .
    ?c2   a                     :WellboreInterval .
    ?c3   a                     :Core .
    ?c8   a                     :WellboreInterval .
    ?c4   a                     :CoreSample .
    ?c6   a                     :CoreSample .
    ?c9   a                     :StratigraphicUnit .
    ?c5   a                     :Permeability .
    ?c7   a                     :Porosity .
    ?c10  a                     :StratigraphicColumn ;
          a                     :LithoStratigraphicColumn .
    ?c1   :hasWellboreInterval  ?c2 .
    ?c2  ^:extractedFrom ?c3 .
    ?c2  :overlapsWellboreInterval  ?c8 .
    ?c3  :hasCoreSample        ?c4 ;
         :hasCoreSample        ?c6 .
    ?c8  :hasUnit              ?c9 .
    ?c4  :hasPermeability  ?c5 .
    ?c6  :hasPorosity  ?c7 .
    ?c9  :ofStratigraphicColumn  ?c10 ;
         :name                 ?a3 .
    ?c5  :valueInStandardUnit  ?a1 .
    ?c7  :valueInOriginalUnit  ?a2
    FILTER regex(?a3, ".*NORDLAND.*")
  }
