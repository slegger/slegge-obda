PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wName ;
           :hasWellboreInterval  ?wi .
    ?c     :extractedFrom        ?wi .
    ?wi    :overlapsWellboreInterval  ?z .
    ?z     :hasUnit              ?u .
    ?u     :name                 ?unit_name .
    ?c     :hasCoreSample        ?s .
    ?s     :name                 ?core_sample_name ;
           :hasPermeability  ?p .
    ?p     :valueInOriginalUnit  ?permeability .
    ?wi    :hasTopDepth  ?td .
    ?td    a                     :MeasuredDepth ;
           :valueInOriginalUnit  ?top_depth .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
