PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wName ;
           :hasWellboreInterval  ?wi .
    ?c     :extractedFrom        ?wi .
    ?wi    :overlapsWellboreInterval  ?z .
    ?z     :hasUnit              ?u .
    ?u     :name                 ?unit_name .
    ?c     :hasCoreSample        ?s .
    ?s     :name                 ?core_sample_name ;
           :hasPermeability  ?p .
    ?p     :valueInOriginalUnit  ?permeability .
    ?wi    :hasTopDepth  ?tmd .
    ?tmd   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?top_md .
    ?wi    :hasTopDepth  ?ttvd .
    ?ttvd  a                     :TrueVerticalDepth ;
           :valueInStandardUnit  ?top_tvd .
    ?wi    :hasBottomDepth  ?bmd .
    ?bmd   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?bot_md .
    ?wi    :hasBottomDepth  ?btvd .
    ?btvd  a                     :TrueVerticalDepth ;
           :valueInStandardUnit  ?bot_tvd
  }
