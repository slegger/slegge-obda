PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?stratigraphic_zone ?fluid ?strat_zone_entry ?strat_zone_exit ?formation_pressure ?pressure_depth ?os
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore ;
              :hasWellboreInterval  ?wi ;
              :hasFormationPressure  ?fp .
    ?fp       :valueInStandardUnit  ?formation_pressure ;
              a                     :FormationPressure ;
              :hasDepth  ?fp_depth .
    ?fp_depth  a                    :MeasuredDepth ;
              :valueInStandardUnit  ?pressure_depth .
    ?wi       :hasTopDepth  ?top ;
              :hasBottomDepth  ?bot ;
              a                     :FluidZone ;
              :name                 ?stratigraphic_zone ;
              :hasUnit              ?strat_unit ;
              :fluidZoneContent     ?fluid ;
              :overlapsWellboreInterval  ?oz .
    ?oz       :hasUnit              ?os .
    ?top      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?strat_zone_entry .
    ?bot      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?strat_zone_exit
    FILTER ( ( ?strat_zone_entry <= ?pressure_depth ) && ( ?strat_zone_exit >= ?pressure_depth ) )
  }
