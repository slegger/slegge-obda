PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wellbore ;
           :hasWellboreInterval  ?ci .
    ?c     :extractedFrom        ?ci .
    ?ci    :hasTopDepth  ?top .
    ?c     :hasCoreSample        ?s .
    ?s     :name                 ?sample_name .
    ?top   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?top_depth .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 62 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
    OPTIONAL
      { ?s  :hasPermeability  ?p .
        ?p  :valueInOriginalUnit  ?permeability
      }
  }
