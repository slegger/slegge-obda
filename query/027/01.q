PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name ?uName
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore_name ;
              :hasWellboreInterval  ?interval .
    ?c        :extractedFrom        ?interval ;
              a                     :Core .
    ?interval  :overlapsWellboreInterval  ?zone .
    ?zone     :hasUnit              ?u .
    ?u        :name                 ?uName
    FILTER regex(?uName, "Tarbert", "i")
  }
