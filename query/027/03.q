PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  *
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wellbore_name ;
           :hasWellboreInterval  ?zone .
    ?zone  :hasUnit              ?u .
    ?u     :name                 ?uName
    FILTER regex(?uName, "Tarbert", "i")
    ?zone     :overlapsWellboreInterval  ?logged_interval1 .
    ?logged_interval1
              :hasLogCurve          ?log1 .
    ?log1     :logCurveNameShort    ?curve_name1 .
    ?zone     :overlapsWellboreInterval  ?logged_interval2 .
    ?logged_interval2
              :hasLogCurve          ?log2 .
    ?log2     :logCurveNameShort    ?curve_name2 .
    ?zone     :overlapsWellboreInterval  ?logged_interval3 .
    ?logged_interval3
              :hasLogCurve          ?log3 .
    ?log3     :logCurveNameShort    ?curve_name3
    FILTER ( ( ( ?curve_name1 = "PHIF" ) && ( ?curve_name2 = "GR" ) ) && ( ?curve_name3 = "VSH" ) )
  }
