PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w     a                     :Wellbore ;
           :hasWellboreInterval  ?zone .
    ?zone  :hasUnit              ?u ;
           :hasTopDepth  ?top ;
           :hasBottomDepth  ?bot .
    ?top   a                     :MeasuredDepth .
    ?bot   a                     :MeasuredDepth .
    ?top   :valueInStandardUnit  ?top_md_m .
    ?bot   :valueInStandardUnit  ?bot_md_m .
    ?u     :name                 ?unit_name
  }
ORDER BY ?unit_name ?w
