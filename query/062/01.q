PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore_name
WHERE
  { ?w  a                     :Wellbore ;
        :name                 ?wellbore_name ;
        :hasWellboreInterval  ?interval .
    ?c  a                     :Core ;
        :extractedFrom        ?interval
  }
