PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?c ?top_tvd_m ?permeability ?lat ?long
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore ;
              :hasWellboreInterval  ?int .
    ?c        a                     :Core ;
              :extractedFrom        ?int .
    ?int      :hasTopDepth  ?top .
    ?top      a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?top_tvd_m .
    ?c        :hasCoreSample        ?s .
    ?s        :hasPermeability  ?p .
    ?p        :valueInStandardUnit  ?permeability .
    ?c        :hasCoreSample        ?s2 .
    ?s2       :hasPorosity  ?por .
    ?por      :valueInStandardUnit  ?porosity .
    ?w        :hasFormationPressure  ?fp .
    ?fp       :valueInStandardUnit  ?formation_pressure ;
              :hasDepth  ?fp_depth .
    ?int      :hasTopDepth  ?top_md ;
              :hasBottomDepth  ?bot_md .
    ?top_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_md_m .
    ?bot_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?bot_md_m .
    ?fp_depth  a                    :MeasuredDepth ;
              :valueInStandardUnit  ?form_press_depth
    FILTER ( ( ?form_press_depth > ?top_md_m ) && ( ?form_press_depth < ?bot_md_m ) )
    ?well  :hasWellbore  ?w ;
           :locatedIn    ?pos .
    ?pos   :latitude     ?lat ;
           :longitude    ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
