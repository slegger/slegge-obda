PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?c ?top_tvd_m ?permeability ?porosity ?lat ?long
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wellbore ;
           :hasWellboreInterval  ?int .
    ?c     a                     :Core ;
           :extractedFrom        ?int .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
    OPTIONAL
      { ?int  :hasTopDepth  ?top .
        ?top  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?top_tvd_m
      }
    OPTIONAL
      { ?c  :hasCoreSample        ?s .
        ?s  :hasPermeability  ?p .
        ?p  :valueInStandardUnit  ?permeability
      }
    OPTIONAL
      { ?c    :hasCoreSample        ?s2 .
        ?s2   :hasPorosity  ?por .
        ?por  :valueInStandardUnit  ?porosity
      }
  }
