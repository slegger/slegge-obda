PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  *
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wName ;
              :hasWellboreInterval  ?cored_int .
    ?c        :extractedFrom        ?cored_int .
    ?cored_int  :hasTopDepth  ?top_md ;
              :hasBottomDepth  ?bot_md .
    ?top_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?top_md_m .
    ?bot_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?bot_md_m
    OPTIONAL
      { ?z  :overlapsWellboreInterval  ?cored_int ;
            :hasUnit              ?u .
        ?u  :name                 ?strat_unit_name
      }
    OPTIONAL
      { ?cored_int  :hasTopDepth  ?top_tvd ;
                  :hasBottomDepth  ?bot_tvd .
        ?top_tvd  a                     :TrueVerticalDepth ;
                  :valueInStandardUnit  ?top_tvd_m .
        ?bot_tvd  a                     :TrueVerticalDepth ;
                  :valueInStandardUnit  ?bot_tvd_m
      }
  }
