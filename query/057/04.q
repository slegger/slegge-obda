PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?stratigraphic_zone ?formation_pressure ?lat ?long
WHERE
  { ?w     a                     :Wellbore ;
           :name                 ?wellbore ;
           :hasWellboreInterval  ?wi ;
           :hasFormationPressure  ?fp .
    ?fp    :valueInStandardUnit  ?formation_pressure .
    ?wi    :hasTopDepth  ?top ;
           :hasBottomDepth  ?bot ;
           a                     :StratigraphicZone ;
           :name                 ?stratigraphic_zone ;
           :hasUnit              ?strat_unit .
    ?top   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?strat_zone_entry .
    ?bot   a                     :MeasuredDepth ;
           :valueInStandardUnit  ?strat_zone_exit .
    ?well  :hasWellbore          ?w ;
           :locatedIn            ?pos .
    ?pos   :latitude             ?lat ;
           :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
