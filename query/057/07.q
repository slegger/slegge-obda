PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?formation_pressure
WHERE
  { ?w   a                     :Wellbore ;
         :name                 ?wellbore ;
         :hasFormationPressure  ?fp .
    ?fp  :valueInStandardUnit  ?formation_pressure
  }
