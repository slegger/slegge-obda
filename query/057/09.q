PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?stratigraphic_zone ?strat_zone_entry ?strat_zone_exit ?formation_pressure ?pressure_depth ?lat ?long ?wkt
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore ;
              :hasWellboreInterval  ?wi ;
              :hasFormationPressure  ?fp .
    ?fp       :valueInStandardUnit  ?formation_pressure ;
              a                     :FormationPressure ;
              :hasDepth  ?fp_depth .
    ?fp_depth  a                    :MeasuredDepth ;
              :valueInStandardUnit  ?pressure_depth .
    ?wi       :hasTopDepth  ?top ;
              :hasBottomDepth  ?bot ;
              a                     :StratigraphicZone ;
              :name                 ?stratigraphic_zone ;
              :hasUnit              ?strat_unit .
    ?top      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?strat_zone_entry .
    ?bot      a                     :MeasuredDepth ;
              :valueInStandardUnit  ?strat_zone_exit
    FILTER ( ( ?strat_zone_entry <= ?pressure_depth ) && ( ?strat_zone_exit >= ?pressure_depth ) )
    ?well  :hasWellbore  ?w ;
           :locatedIn    ?pos .
    ?pos   :latitude     ?lat ;
           :longitude    ?long ;
           :WKT          ?wkt
    FILTER regex(?stratigraphic_zone, "Tarbert Fm.", "i")
  }
