select distinct 
wellbore.wellbore_id, 
'Pressure: ' 
|| p_pressure.data_value 
|| ' ' 
|| p_pressure.data_value_u 
|| ' at depth: '
|| fp_depth_pt1_loc.data_value_1_o 
|| ' ' 
|| fp_depth_pt1_loc.data_value_1_ou as formation_pressure ,
strat_zone.STRAT_ZONE_IDENTIFIER,
' Top: MD: '
|| strat_zone.strat_zone_entry_md
|| strat_zone.strat_zone_depth_uom
|| ' TVD: '
|| SLEGGE.FRAME_LEAF.MDTVD(wellbore.wellbore_id, strat_zone.strat_zone_entry_md)
|| strat_zone.strat_zone_depth_uom
|| ' Base: MD: ' 
|| strat_zone.strat_zone_exit_md
|| strat_zone.strat_zone_depth_uom 
|| ' TVD: '
|| SLEGGE.FRAME_LEAF.MDTVD(wellbore.wellbore_id, strat_zone.strat_zone_exit_md)
 || strat_zone.strat_zone_depth_uom as STRAT_ZONE_INFO,
reservoir_top.STRAT_PICK_IDENTIFIER 
|| ' Depth: '
|| reservoir_top.pick_depth
 || reservoir_top.pick_depth_uom,
reservoir_base.strat_pick_identifier || ' Depth: ' || reservoir_base.pick_depth, 
'Gross thickness (md) ' || (reservoir_base.pick_depth - reservoir_top.pick_depth)
 || reservoir_top.pick_depth_uom
|| 'Gross thickness (tvd) ' || (SLEGGE.FRAME_LEAF.MDTVD(reservoir_top.wellbore, reservoir_base.pick_depth) - SLEGGE.FRAME_LEAF.MDTVD(reservoir_top.wellbore, reservoir_top.pick_depth))
 || reservoir_top.pick_depth_uom
from 
slegge_epi.p_pressure, 
slegge_epi.activity fp_depth_data,
slegge_epi.p_location_1d fp_depth_pt1_loc, 
slegge_epi.wellbore,
slegge_epi.activity_class form_pressure_class,
SLEGGE.STRATIGRAPHIC_PICKS reservoir_top,
slegge.stratigraphic_picks reservoir_base,
SLEGGE.PICKED_STRATIGRAPHIC_ZONES strat_zone
where 
p_pressure.activity_s = fp_depth_data.activity_s 
and fp_depth_data.activity_s = fp_depth_pt1_loc.activity_s 
and fp_depth_data.facility_s = wellbore.wellbore_s
and fp_depth_data.kind_s = form_pressure_class.activity_class_s
and form_pressure_class.clsn_cls_name = 'formation pressure depth data' and
reservoir_top.wellbore = wellbore.wellbore_id and
reservoir_base.wellbore = wellbore.wellbore_id and
reservoir_top.STRAT_UNIT_IDENTIFIER IN ('Top Gas', 'Top Oil') and
reservoir_top.STRAT_COLUMN_IDENTIFIER = 'FLUID' and
reservoir_base.STRAT_COLUMN_IDENTIFIER = 'FLUID' and
reservoir_base.pick_depth > reservoir_top.pick_depth and
strat_zone.wellbore = wellbore.wellbore_id and
reservoir_top.pick_depth_uom = strat_zone.strat_zone_depth_uom and
reservoir_top.pick_depth >= strat_zone.strat_zone_entry_md and 
reservoir_top.pick_depth <= strat_zone.strat_zone_exit_md and
strat_zone.strat_zone_entry_md <= fp_depth_pt1_loc.data_value_1_o and
strat_zone.strat_zone_exit_md >= fp_depth_pt1_loc.data_value_1_o and
/* NOTE: The line below represents the input to the query */
strat_zone.strat_unit_identifier LIKE 'Tarbert%';
/*GROUP BY (wellbore.wellbore_id, reservoir_base.strat_pick_identifier, p_pressure);*/
