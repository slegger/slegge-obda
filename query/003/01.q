PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT DISTINCT  ?wellbore ?content
WHERE
  { ?w    a                     :Wellbore ;
          :name                 ?wellbore ;
          :hasWellboreInterval  ?int .
    ?int  a                     :FluidZone ;
          :fluidZoneContent     ?content
  }
