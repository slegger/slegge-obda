PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?unit_name ?discovery
WHERE
  { ?w       a                     :Wellbore ;
             :name                 ?wellbore ;
             :hasWellboreInterval  ?c_int ;
             :hasWellboreInterval  ?f_int .
    ?c_int   :hasUnit              ?c_unit .
    ?c_unit  :name                 ?unit_name .
    ?f_int   a                     :FluidZone ;
             :name                 ?discovery ;
             :overlapsWellboreInterval  ?c_int
  }
