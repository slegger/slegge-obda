PREFIX  :     <http://slegger.gitlab.io/slegge-obda/ontology/subsurface-exploration#>

SELECT  ?wellbore ?unit_name ?discovery ?chrono_top_depth_md_m ?lat ?long
WHERE
  { ?w        a                     :Wellbore ;
              :name                 ?wellbore ;
              :hasWellboreInterval  ?c_int .
    ?c_int    :hasUnit              ?c_unit .
    ?c_unit   :name                 ?unit_name .
    ?f_int    a                     :FluidZone ;
              :name                 ?discovery ;
              :overlapsWellboreInterval  ?c_int .
    ?c_int    :hasTopDepth  ?top_md .
    ?top_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?chrono_top_depth_md_m .
    ?c_int    :hasBottomDepth  ?bot_md .
    ?bot_md   a                     :MeasuredDepth ;
              :valueInStandardUnit  ?chrono_bot_depth_md_m .
    ?c_int    :hasTopDepth  ?top_tvd .
    ?top_tvd  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?chrono_top_depth_tvd_m .
    ?c_int    :hasBottomDepth  ?bot_tvd .
    ?bot_tvd  a                     :TrueVerticalDepth ;
              :valueInStandardUnit  ?chrono_bot_depth_tvd_m .
    ?well     :hasWellbore          ?w ;
              :locatedIn            ?pos .
    ?pos      :latitude             ?lat ;
              :longitude            ?long
    FILTER ( ( ( ( ?lat > 60 ) && ( ?lat < 61 ) ) && ( ?long > 2 ) ) && ( ?long < 3 ) )
  }
ORDER BY ?discovery
